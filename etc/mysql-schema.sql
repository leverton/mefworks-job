CREATE TABLE `job` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`uuid` CHAR(22) NOT NULL DEFAULT '' COLLATE 'latin1_bin',
	`name` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'latin1_swedish_ci',
	`status` ENUM('queued','running','done') NOT NULL DEFAULT 'queued',
	`payload` TEXT NULL,
	`is_successful` TINYINT(1) UNSIGNED NULL DEFAULT NULL,
	`response` TEXT NULL,
	`recur` TEXT NULL,
	`created_dt` DATETIME NOT NULL,
	`scheduled_dt` DATETIME NOT NULL,
	`started_dt` DATETIME NULL DEFAULT NULL,
	`run_time` FLOAT NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `uuid` (`uuid`),
	INDEX `status` (`status`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
