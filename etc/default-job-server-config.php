<?php

use mef\Db\Driver\PdoDriver;
use mef\Db\TransactionDriver\NestedTransactionDriver;
use mef\Job\JobStore\DatabaseJobStore;
use mef\Job\JobQueue\JobServer;
use mef\Job\JobHandler\PhpJobHandler;
use mef\Job\WorkerFactory\PimpleWorkerFactory;
use mef\Log\StandardLogger;

return [
	'pimple' => [
		'services' => [
			'job-store' => function($container) {
				return new DatabaseJobStore($container['database']);
			},
			'job-server' => function($container) {
				return new JobServer($container['job-store'], $container['job-id-generator']);
			},
			'job-handler' => function($container) {
				return new PhpJobHandler($container['worker-factory'], $container['jobs-to-run']);
			},
			'worker-factory' => function($container) {
				return new PimpleWorkerFactory($container, function($jobName) use ($container) {
					return $container['worker-namespace'] . str_replace(' ', '', ucwords(str_replace('-',' ',$jobName))) . 'Worker';
				});
			},
			'logger' => function($container) {
				return new StandardLogger;
			}
		],

		'factories' => [
			'database' => function($container) {
				$pdo = new PDO('mysql:dbname=test', 'root', '');
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$db = new PdoDriver($pdo);
				$db->setTransactionDriver(new NestedTransactionDriver($db));
				return $db;
			}
		],

		'parameters' => [
			'job-id-generator' => uniqid(...),
			'worker-namespace' => 'mef\\Job\\Worker\\',
			'jobs-to-run' => ['hello']
		]
	]
];