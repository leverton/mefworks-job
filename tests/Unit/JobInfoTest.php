<?php namespace Test\Unit;

use mef\Job\JobInfo;
use mef\Job\JobInfoInterface;

/**
 * @coversDefaultClass \mef\Job\JobInfo
 */
class JobInfoTest extends \PHPUnit\Framework\TestCase
{
	const JOB_INFO_ID      = 'job-id';
	const JOB_INFO_NAME    = 'job-name';
	const JOB_INFO_PAYLOAD = ['key' => 'value'];

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$jobInfo = new JobInfo(self::JOB_INFO_ID, self::JOB_INFO_NAME, self::JOB_INFO_PAYLOAD);

		$this->assertInstanceOf(JobInfoInterface::class, $jobInfo);

		return $jobInfo;
	}

	/**
	 * @covers ::getId
	 * @covers ::getName
	 * @covers ::getPayload
	 * @depends testConstructor
	 */
	public function testGetters(JobInfo $jobInfo)
	{
		$this->assertEquals(self::JOB_INFO_ID, $jobInfo->getId());
		$this->assertEquals(self::JOB_INFO_NAME, $jobInfo->getName());
		$this->assertEquals(self::JOB_INFO_PAYLOAD, $jobInfo->getPayload());
	}
}
