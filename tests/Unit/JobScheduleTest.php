<?php namespace Test\Unit;

use mef\Job\JobSchedule;
use mef\Job\JobScheduleInterface;
use mef\Job\DateSequence\DateSequenceInterface;

/**
 * @coversDefaultClass \mef\Job\JobSchedule
 */
class JobScheduleTest extends \PHPUnit\Framework\TestCase
{
	const JOB_SCHEDULE_DATETIME = '2015-01-01';

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$dateSequence = $this->getMockBuilder(DateSequenceInterface::class)->getMock();

		$jobSchedule = new JobSchedule(
			new \DateTimeImmutable(self::JOB_SCHEDULE_DATETIME),
			$dateSequence
		);

		$this->assertInstanceOf(JobScheduleInterface::class, $jobSchedule);

		return $jobSchedule;
	}

	/**
	 * @covers ::getScheduledDateTime
	 * @covers ::getRepeatSequence
	 * @depends testConstructor
	 */
	public function testGetters(JobSchedule $jobSchedule)
	{
		$this->assertEquals(SELF::JOB_SCHEDULE_DATETIME, $jobSchedule->getScheduledDateTime()->format('Y-m-d'));
		$this->assertTrue($jobSchedule->getRepeatSequence() instanceof DateSequenceInterface);
	}
}
