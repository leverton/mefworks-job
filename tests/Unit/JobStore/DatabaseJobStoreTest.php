<?php namespace Test\Unit\JobQueue;

use Exception;
use mef\Job\JobStore\DatabaseJobStore;
use mef\Job\JobStore\JobStoreInterface;
use mef\Job\JobResult;
use TypeError;

/**
 * @coversDefaultClass \mef\Job\JobStore\DatabaseJobStore
 */
class DatabaseJobStoreTest extends \PHPUnit\Framework\TestCase
{
	private $transactionDriver;

	public function setUp() : void
	{
		$this->transactionDriver = $this->getMockBuilder('\mef\Db\TransactionDriver\TransactionDriverInterface')->getMock();
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{

		$jobStore = new DatabaseJobStore($this->getDriver());

		$this->assertInstanceOf(JobStoreInterface::class, $jobStore);

		return $jobStore;
	}

	/**
	 * @covers ::addJob
	 * @covers ::getUTCTime
	 * @depends testConstructor
	 */
	public function testAddJob(DatabaseJobStore $jobStore)
	{
		$info = $this->getMockBuilder('\mef\Job\JobInfoInterface')->getMock();
		$schedule = $this->getMockBuilder('\mef\Job\JobScheduleInterface')->getMock();
		$schedule->expects($this->once())->method('getScheduledDateTime')->willReturn(new \DateTimeImmutable);

		$jobStore->addJob($info, $schedule);
	}

	/**
	 * @covers ::cancelJob
	 * @depends testConstructor
	 */
	public function testCancelJob(DatabaseJobStore $jobStore)
	{
		$this->transactionDriver->expects($this->once())->method('start');
		$this->transactionDriver->expects($this->once())->method('commit');

		$driver = $this->getDriver([['status' => 'queued', 'id' => 1]]);
		$driver->setTransactionDriver($this->transactionDriver);

		$jobStore = new DatabaseJobStore($driver);

		$jobStore->cancelJob('some-job');
	}

	/**
	 * @covers ::cancelJob
	 * @depends testConstructor
	 */
	public function testCancelJobNoJobFailure(DatabaseJobStore $jobStore)
	{
		$this->expectException(Exception::class);
		$this->transactionDriver->expects($this->once())->method('start');
		$this->transactionDriver->expects($this->never())->method('commit');
		$this->transactionDriver->expects($this->once())->method('rollback');

		$driver = $this->getDriver();
		$driver->setTransactionDriver($this->transactionDriver);

		$jobStore = new DatabaseJobStore($driver);

		$jobStore->cancelJob('some-job');
	}

	/**
	 * @covers ::cancelJob
	 * @depends testConstructor
	 */
	public function testCancelJobNotQueuedJobFailure(DatabaseJobStore $jobStore)
	{
		$this->expectException(Exception::class);

		$this->transactionDriver->expects($this->once())->method('start');
		$this->transactionDriver->expects($this->never())->method('commit');
		$this->transactionDriver->expects($this->once())->method('rollback');

		$driver = $this->getDriver([['status' => 'running', 'id' => 1]]);
		$driver->setTransactionDriver($this->transactionDriver);

		$jobStore = new DatabaseJobStore($driver);

		$jobStore->cancelJob('some-job');
	}

	/**
	 * @covers ::startNextJob
	 * @depends testConstructor
	 */
	public function testStartNextJob(DatabaseJobStore $jobStore)
	{
		$this->transactionDriver->expects($this->once())->method('start');
		$this->transactionDriver->expects($this->once())->method('commit');

		$driver = $this->getDriver([
			['uuid' => 'job-1-uuid', 'id' => 1],
			['uuis' => 'job-2-uuid', 'id' => 2]
		]);
		$driver->setTransactionDriver($this->transactionDriver);

		$jobStore = new DatabaseJobStore($driver);

		$jobStore->startNextJob(['some-job', 'some-other-job']);
	}

	/**
	 * @covers ::startNextJob
	 * @depends testConstructor
	 */
	public function testStartNextJobEmptyQueueFailure(DatabaseJobStore $jobStore)
	{
		$this->expectException(Exception::class);

		$this->transactionDriver->expects($this->once())->method('start');
		$this->transactionDriver->expects($this->never())->method('commit');
		$this->transactionDriver->expects($this->once())->method('rollback');

		$driver = $this->getDriver([]);
		$driver->setTransactionDriver($this->transactionDriver);

		$jobStore = new DatabaseJobStore($driver);

		$jobStore->startNextJob(['some-job']);
	}

	/**
	 * @covers ::finishJob
	 * @depends testConstructor
	 */
	public function testFinishJob(DatabaseJobStore $jobStore)
	{
		$this->transactionDriver->expects($this->once())->method('start');
		$this->transactionDriver->expects($this->once())->method('commit');

		$driver = $this->getDriver([['status' => 'running', 'id' => 1]]);
		$driver->setTransactionDriver($this->transactionDriver);

		$jobStore = new DatabaseJobStore($driver);

		$result = $this->getMockBuilder('mef\job\JobResultInterface')->getMock();
		$result->expects($this->once())->method('isSuccessful');
		$result->expects($this->once())->method('getResponse');
		$result->expects($this->once())->method('getRunTime');

		$jobStore->finishJob('some-job', $result);
	}

	/**
	 * @covers ::finishJob
	 * @depends testConstructor
	 */
	public function testFinishJobNoJobFailure(DatabaseJobStore $jobStore)
	{
		$this->expectException(Exception::class);

		$this->transactionDriver->expects($this->once())->method('start');
		$this->transactionDriver->expects($this->never())->method('commit');
		$this->transactionDriver->expects($this->once())->method('rollback');

		$driver = $this->getDriver();
		$driver->setTransactionDriver($this->transactionDriver);

		$jobStore = new DatabaseJobStore($driver);

		$result = $this->getMockBuilder('mef\job\JobResultInterface')->getMock();

		$jobStore->finishJob('some-job', $result);
	}

	/**
	 * @covers ::finishJob
	 * @depends testConstructor
	 */
	public function testFinishJobNotRunnningJobFailure(DatabaseJobStore $jobStore)
	{
		$this->expectException(Exception::class);

		$this->transactionDriver->expects($this->once())->method('start');
		$this->transactionDriver->expects($this->never())->method('commit');
		$this->transactionDriver->expects($this->once())->method('rollback');

		$driver = $this->getDriver([['status' => 'queued']]);
		$driver->setTransactionDriver($this->transactionDriver);

		$jobStore = new DatabaseJobStore($driver);

		$result = $this->getMockBuilder('mef\job\JobResultInterface')->getMock();

		$jobStore->finishJob('some-job', $result);
	}

	private function getDriver($rows = [])
	{
		$dataProvider = new \mef\Db\Driver\DataProvider\SequentialArrayDataProvider([$rows]);

		return new \mef\Db\Driver\DataProviderDriver($dataProvider);
	}

	/**
	 * @covers ::getJob
	 * @depends testConstructor
	 */
	public function testGetJob(DatabaseJobStore $jobStore)
	{
		$driver = $this->getDriver([
			[
				'status' => 'queued',
				'uuid' => 'some-uuid',
				'name' => 'some-name',
				'payload' => '',
				'scheduled_dt' => '',
				'recur' => ''
			]
		]);
		$jobStore = new DatabaseJobStore($driver);
		$result = $jobStore->getJob('some-job');
		$this->assertNull($result->getResult());

		$driver = $this->getDriver([
			[
				'status' => 'done',
				'uuid' => 'some-uuid',
				'name' => 'some-name',
				'payload' => '',
				'scheduled_dt' => '',
				'recur' => '',
				'is_successful' => '',
				'response' => '',
				'run_time' => ''
			]
		]);
		$jobStore = new DatabaseJobStore($driver);
		$result = $jobStore->getJob('some-job');
		$this->assertInstanceOf(JobResult::class, $result->getResult());
	}

	/**
	 * @covers ::getJob
	 * @depends testConstructor
	 */
	public function testGetJobNoDataFailure(DatabaseJobStore $jobStore)
	{
		$this->expectException(Exception::class);

		$driver = $this->getDriver();
		$jobStore = new DatabaseJobStore($driver);
		$jobStore->getJob('some-job');
	}
}
