<?php namespace Test\Unit\Exception;

use mef\Job\Exception\RetryException;
use mef\Job\Exception\AbstractWorkerResponseException;

/**
 * @coversDefaultClass \mef\Job\Exception\RetryException
 */
class RetryExceptionTest extends \PHPUnit\Framework\TestCase
{
	private $rescheduleDate;

	public function setUp() : void
	{
		$this->rescheduleDate = new \DateTimeImmutable('2015-03-01');
	}
	/**
	 * @covers ::__construct
	 * @covers \mef\Job\Exception\AbstractWorkerResponseException::__construct
	 */
	public function testConstructor()
	{
		$exception = new RetryException($this->rescheduleDate, 'Test Message');

		$this->assertInstanceOf(AbstractWorkerResponseException::class, $exception);

		return $exception;
	}

	/**
	 * @covers \mef\Job\Exception\AbstractWorkerResponseException::getResponse
	 * @depends testConstructor
	 */
	public function testGetResponse(RetryException $exception)
	{
		$response = $exception->getResponse();
		$this->assertEquals($this->rescheduleDate->format('c'), $response['reschedule']);
	}

	/**
	 * @covers ::getRescheduleDate
	 * @depends testConstructor
	 */
	public function testGetRescheduleDate(RetryException $exception)
	{
		$date = $exception->getRescheduleDate();
		$this->assertEquals($this->rescheduleDate->format('c'), $date->format('c'));
	}
}
