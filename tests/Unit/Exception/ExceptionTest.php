<?php namespace Test\Unit\Exception;

use mef\Job\Exception\Exception;

/**
 * @coversDefaultClass \mef\Job\Exception\Exception
 */
class ExceptionTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$exception = new Exception('Test Message');

		$this->assertInstanceOf(\Exception::class, $exception);
	}
}
