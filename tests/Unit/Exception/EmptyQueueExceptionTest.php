<?php namespace Test\Unit\Exception;

use mef\Job\Exception\EmptyQueueException;
use mef\Job\Exception\Exception;

/**
 * @coversDefaultClass \mef\Job\Exception\EmptyQueueException
 */
class EmptyQueueExceptionTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$exception = new EmptyQueueException();

		$this->assertInstanceOf(Exception::class, $exception);
	}
}
