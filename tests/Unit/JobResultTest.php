<?php namespace Test\Unit;

use mef\Job\JobResult;
use mef\Job\JobResultInterface;

/**
 * @coversDefaultClass \mef\Job\JobResult
 */
class JobResultTest extends \PHPUnit\Framework\TestCase
{
	const JOB_RESULT_IS_SUCCESSFUL = true;
	const JOB_RESULT_RESPONSE = ['key', 'value'];
	const JOB_RESULT_RUNTIME = 0.05;

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$jobResult = new JobResult(SELF::JOB_RESULT_IS_SUCCESSFUL, SELF::JOB_RESULT_RESPONSE, SELF::JOB_RESULT_RUNTIME);

		$this->assertInstanceOf(JobResultInterface::class, $jobResult);

		return $jobResult;
	}

	/**
	 * @covers ::isSuccessful
	 * @covers ::getResponse
	 * @covers ::getRunTime
	 * @depends testConstructor
	 */
	public function testGetters(JobResult $jobResult)
	{
		$this->assertEquals(SELF::JOB_RESULT_IS_SUCCESSFUL, $jobResult->isSuccessful());
		$this->assertEquals(SELF::JOB_RESULT_RESPONSE, $jobResult->getResponse());
		$this->assertEquals(SELF::JOB_RESULT_RUNTIME, $jobResult->getRunTime());
	}
}
