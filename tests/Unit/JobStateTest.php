<?php namespace Test\Unit;

use mef\Job\JobState;

/**
 * @coversDefaultClass \mef\Job\JobState
 */
class JobStateTest extends \PHPUnit\Framework\TestCase
{
	const JOB_STATES = ['queued', 'running', 'done'];

	/**
	 * @coversNothing
	 */
	public function testJobStates()
	{
		foreach (self::JOB_STATES as $jobState)
		{
			$this->assertNotNull(JobState::tryFrom($jobState));
		}
	}
}
