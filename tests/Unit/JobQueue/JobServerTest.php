<?php namespace Test\Unit\JobQueue;

use mef\Job\JobInterface;
use mef\Job\JobState;
use mef\Job\JobQueue\JobServerInterface;
use mef\Job\JobQueue\JobServer;
use mef\Job\JobQueue\JobQueueInterface;
use mef\Job\JobStore\JobStoreInterface;

/**
 * @coversDefaultClass \mef\Job\JobQueue\JobServer
 */
class JobServerTest extends \PHPUnit\Framework\TestCase
{
	private $jobStore;
	private $idGen;

	const JOB_ID = 'uniqueid';

	public function setUp() : void
	{
		$this->jobStore = $this->getMockBuilder('\mef\Job\JobStore\JobStoreInterface')->getMock();

		$this->idGen = function () {
			return self::JOB_ID;
		};
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$jobServer = new JobServer($this->jobStore, $this->idGen);

		$this->assertInstanceOf(JobServerInterface::class, $jobServer);
		$this->assertInstanceOf(JobQueueInterface::class, $jobServer);

		return $jobServer;
	}

	/**
	 * @covers ::getJobStore
	 * @depends testConstructor
	 */
	public function testGetJobStore(JobServer $jobServer)
	{
		$jobStore = $jobServer->getJobStore();

		$this->assertInstanceOf(JobStoreInterface::class, $jobStore);
		$this->assertEquals($this->jobStore, $jobStore);

		return $jobServer;
	}

	/**
	 * @covers ::enqueue
	 * @covers ::scheduleJob
	 * @depends testConstructor
	 */
	public function testEnqueue(JobServer $jobServer)
	{
		$this->jobStore->expects($this->once())->method('addJob');
		$jobServer = new JobServer($this->jobStore, $this->idGen);
		$jobId = $jobServer->enqueue('test-job', ['test' => 'payload']);

		$this->assertEquals(self::JOB_ID, $jobId);

		return $jobId;
	}

	/**
	 * @covers ::dequeue
	 * @depends testConstructor
	 * @depends testEnqueue
	 */
	public function testDequeue($jobServer, $jobId)
	{
		$this->jobStore->expects($this->once())->method('cancelJob');
		$jobServer = new JobServer($this->jobStore, $this->idGen);
		$jobServer->dequeue($jobId);
	}

	/**
	 * @covers ::isQueued
	 * @depends testConstructor
	 * @depends testEnqueue
	 */
	public function testIsQueuedQueuedJob($jobServer, $jobId)
	{
		$jobStatus = $this->getMockBuilder('\mef\Job\JobStatusInterface')->getMock();
		$jobStatus->expects($this->once())->method('getState')->willReturn(JobState::QUEUED);

		$job = $this->getMockBuilder('\mef\Job\JobInterface')->getMock();
		$job->expects($this->once())->method('getStatus')->willReturn($jobStatus);

		$this->jobStore->expects($this->once())->
			method('getJob')->
			willReturn($job);

		$jobServer = new JobServer($this->jobStore, $this->idGen);
		$isQueued = $jobServer->isQueued($jobId);

		$this->assertTrue($isQueued);
	}

	/**
	 * @covers ::isQueued
	 * @depends testConstructor
	 * @depends testEnqueue
	 */
	public function testIsQueuedRunningJob($jobServer, $jobId)
	{
		$jobStatus = $this->getMockBuilder('\mef\Job\JobStatusInterface')->getMock();
		$jobStatus->expects($this->once())->method('getState')->willReturn(JobState::RUNNING);

		$job = $this->getMockBuilder('\mef\Job\JobInterface')->getMock();
		$job->expects($this->once())->method('getStatus')->willReturn($jobStatus);

		$this->jobStore->expects($this->once())->
			method('getJob')->
			willReturn($job);

		$jobServer = new JobServer($this->jobStore, $this->idGen);
		$isQueued = $jobServer->isQueued($jobId);

		$this->assertTrue($isQueued);
	}

	/**
	 * @covers ::isQueued
	 * @depends testConstructor
	 * @depends testEnqueue
	 */
	public function testIsQueuedDoneJob($jobServer, $jobId)
	{
		$jobStatus = $this->getMockBuilder('\mef\Job\JobStatusInterface')->getMock();
		$jobStatus->expects($this->once())->method('getState')->willReturn(JobState::DONE);

		$job = $this->getMockBuilder('\mef\Job\JobInterface')->getMock();
		$job->expects($this->once())->method('getStatus')->willReturn($jobStatus);

		$this->jobStore->expects($this->once())->
			method('getJob')->
			willReturn($job);

		$jobServer = new JobServer($this->jobStore, $this->idGen);
		$isQueued = $jobServer->isQueued($jobId);

		$this->assertFalse($isQueued);
	}

	/**
	 * @covers ::isQueued
	 * @depends testConstructor
	 */
	public function testIsQueuedHandleFailure($jobServer)
	{
		$this->jobStore->expects($this->once())->
			method('getJob')->
			will($this->throwException(new \Exception));

		$jobServer = new JobServer($this->jobStore, $this->idGen);
		$isQueued = $jobServer->isQueued('some-job');

		$this->assertFalse($isQueued);
	}

	/**
	 * @covers ::fetchJob
	 * @depends testConstructor
	 */
	public function testFetchJob($jobServer)
	{
		$this->jobStore->expects($this->once())->
			method('getJob')->willReturn($this->getMockBuilder(JobInterface::class)->getMock());

		$jobServer = new JobServer($this->jobStore, $this->idGen);
		$jobServer->fetchJob('some-job');
	}
}
