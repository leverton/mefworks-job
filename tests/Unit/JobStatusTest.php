<?php namespace Test\Unit;

use mef\Job\JobState;
use mef\Job\JobStatus;
use mef\Job\JobStatusInterface;

/**
 * @coversDefaultClass \mef\Job\JobStatus
 */
class JobStatusTest extends \PHPUnit\Framework\TestCase
{
	const JOB_STATUS_STARTE_DATETIME = '2015-01-01';
	const JOB_STATUS_STATE = JobState::RUNNING;

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$jobStatus = new JobStatus(self::JOB_STATUS_STATE, new \DateTimeImmutable(self::JOB_STATUS_STARTE_DATETIME));

		$this->assertInstanceOf(JobStatusInterface::class, $jobStatus);

		return $jobStatus;
	}

	/**
	 * @covers ::getState
	 * @covers ::getStartedDateTime
	 * @depends testConstructor
	 */
	public function testGetters(JobStatus $jobStatus)
	{
		$this->assertEquals(self::JOB_STATUS_STATE, $jobStatus->getState());
		$this->assertEquals(self::JOB_STATUS_STARTE_DATETIME, $jobStatus->getStartedDateTime()->format('Y-m-d'));
	}
}
