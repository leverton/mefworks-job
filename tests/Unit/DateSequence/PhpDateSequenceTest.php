<?php namespace Test\Unit\Exception;

use DateTimeImmutable;
use DateTimeZone;
use InvalidArgumentException;
use Iterator;
use LimitIterator;
use mef\Job\DateSequence\PhpDateSequence;

/**
 * @coversDefaultClass \mef\Job\DateSequence\PhpDateSequence
 */
class PhpDateSequenceTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getFormat
	 */
	public function testGetFormat()
	{
		$format = '+10 days';

		$sequence = new PhpDateSequence($format);
		$this->assertSame($format, $sequence->getFormat());
	}

	/**
	 * @covers ::__construct
	 * @covers ::getTimeZone
	 */
	public function testGetTimeZone()
	{
		$format = '+10 days';
		$timeZone = new DateTimeZone('America/Chicago');

		$sequence = new PhpDateSequence($format, $timeZone);
		$this->assertSame($timeZone->getName(), $sequence->getTimeZone()->getName());
	}

	/**
	 * @covers ::getNextDate
	 * @covers ::getDateAdjustedForTimeZone
	 */
	public function testNextDate()
	{
		$format = '+1 day';

		$sequence = new PhpDateSequence($format);
		$nextDate = $sequence->getNextDate(new DateTimeImmutable('2000-01-01 00:00:00 UTC'));

		$this->assertSame('2000-01-02 00:00:00', $nextDate->format('Y-m-d H:i:s'));
	}

	/**
	 * @covers ::getNextDate
	 * @covers ::getDateAdjustedForTimeZone
	 */
	public function testNextDateWithTimeZone()
	{
		$format = '+1 day';
		$timeZone = new DateTimeZone('America/Chicago');

		$sequence = new PhpDateSequence($format, $timeZone);
		$nextDate = $sequence->getNextDate(new DateTimeImmutable('2000-01-01 00:00:00 UTC'));

		$this->assertSame('2000-01-01 18:00:00', $nextDate->format('Y-m-d H:i:s'));
	}

	/**
	 * @covers ::getIterator
	 * @covers ::getDateAdjustedForTimeZone
	 */
	public function testGetIterator()
	{
		$format = '+1 day';

		$sequence = new PhpDateSequence($format);

		$startDate = new DateTimeImmutable('2000-01-01 00:00:00 UTC');
		$iterator = $sequence->getIterator($startDate);

		$this->assertTrue($iterator instanceof Iterator);

		$dates = iterator_to_array(new LimitIterator($iterator, 0, 10));

		$this->assertEquals($dates[0], $startDate);
		$this->assertSame('2000-01-02 00:00:00', $dates[1]->format('Y-m-d H:i:s'));
		$this->assertSame('2000-01-10 00:00:00', $dates[9]->format('Y-m-d H:i:s'));
	}

	/**
	 * @covers ::getIterator
	 * @covers ::getDateAdjustedForTimeZone
	 */
	public function testGetIteratorWithTimeZone()
	{
		$format = '+1 day';
		$timeZone = new DateTimeZone('America/Chicago');

		$sequence = new PhpDateSequence($format, $timeZone);

		$startDate = new DateTimeImmutable('2000-01-01 00:00:00 UTC');
		$iterator = $sequence->getIterator($startDate);

		$this->assertTrue($iterator instanceof Iterator);

		$dates = iterator_to_array(new LimitIterator($iterator, 0, 10));

		$this->assertSame('1999-12-31 18:00:00', $dates[0]->format('Y-m-d H:i:s'));
		$this->assertSame('2000-01-01 18:00:00', $dates[1]->format('Y-m-d H:i:s'));
		$this->assertSame('2000-01-09 18:00:00', $dates[9]->format('Y-m-d H:i:s'));
	}

	/**
	 * @covers ::serialize
	 * @covers ::unserialize
	 */
	public function testSerialization()
	{
		$format = '+1 day';

		$sequence1 = new PhpDateSequence($format);
		$sequence2 = unserialize(serialize($sequence1));

		$this->assertSame($sequence1->getFormat(), $sequence2->getFormat());
	}

	/**
	 * @covers ::serialize
	 * @covers ::unserialize
	 */
	public function testSerializationWithTimeZone()
	{
		$format = '+1 day';
		$timeZone = new DateTimeZone('America/Chicago');

		$sequence1 = new PhpDateSequence($format, $timeZone);
		$sequence2 = unserialize(serialize($sequence1));

		$this->assertSame($sequence1->getFormat(), $sequence2->getFormat());
		$this->assertSame($sequence1->getTimeZone()->getName(), $sequence2->getTimeZone()->getName());
	}
}
