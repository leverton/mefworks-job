<?php namespace Test\Unit\Exception;

use DateTimeImmutable;
use Iterator;
use LimitIterator;
use mef\Job\DateSequence\DurationDateSequence;

/**
 * @coversDefaultClass \mef\Job\DateSequence\DurationDateSequence
 */
class DurationDateSequenceTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getFormat
	 */
	public function testGetFormat()
	{
		$format = 'P10D';

		$sequence = new DurationDateSequence($format);
		$this->assertSame($format, $sequence->getFormat());
	}

	/**
	 * @covers ::getNextDate
	 */
	public function testNextDate()
	{
		$format = 'P1D';

		$sequence = new DurationDateSequence($format);
		$nextDate = $sequence->getNextDate(new DateTimeImmutable('2000-01-01 00:00:00 UTC'));

		$this->assertSame('2000-01-02 00:00:00', $nextDate->format('Y-m-d H:i:s'));
	}

	/**
	 * @covers ::getIterator
	 */
	public function testGetIterator()
	{
		$format = 'P1D';

		$sequence = new DurationDateSequence($format);

		$startDate = new DateTimeImmutable('2000-01-01 00:00:00 UTC');
		$iterator = $sequence->getIterator($startDate);

		$this->assertTrue($iterator instanceof Iterator);

		$dates = iterator_to_array(new LimitIterator($iterator, 0, 10));

		$this->assertEquals($dates[0], $startDate);
		$this->assertSame('2000-01-02 00:00:00', $dates[1]->format('Y-m-d H:i:s'));
		$this->assertSame('2000-01-10 00:00:00', $dates[9]->format('Y-m-d H:i:s'));
	}

	/**
	 * @covers ::serialize
	 * @covers ::unserialize
	 */
	public function testSerialization()
	{
		$format = 'P1D';

		$sequence1 = new DurationDateSequence($format);
		$sequence2 = unserialize(serialize($sequence1));

		$this->assertSame($sequence1->getFormat(), $sequence2->getFormat());
	}
}
