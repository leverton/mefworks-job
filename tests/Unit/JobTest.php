<?php namespace Test\Unit;

use mef\Job\Job;
use mef\Job\JobInterface;

/**
 * @coversDefaultClass \mef\Job\Job
 */
class JobTest extends \PHPUnit\Framework\TestCase
{
	public function setUp() : void
	{
		$this->info = $this->getMockBuilder('\mef\Job\JobInfoInterface')->getMock();
		$this->status = $this->getMockBuilder('\mef\Job\JobStatusInterface')->getMock();
		$this->schedule = $this->getMockBuilder('\mef\Job\JobScheduleInterface')->getMock();
		$this->result = $this->getMockBuilder('\mef\Job\JobResultInterface')->getMock();
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$job = new Job($this->info, $this->status, $this->schedule, $this->result);

		$this->assertInstanceOf(JobInterface::class, $job);

		return $job;
	}

	/**
	 * @covers ::getInfo
	 * @covers ::getStatus
	 * @covers ::getResult
	 * @covers ::getSchedule
	 * @depends testConstructor
	 */
	public function testGetters(Job $job)
	{
		$this->assertEquals($this->info, $job->getInfo());
		$this->assertEquals($this->status, $job->getStatus());
		$this->assertEquals($this->result, $job->getResult());
		$this->assertEquals($this->schedule, $job->getSchedule());
	}
}
