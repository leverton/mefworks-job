<?php namespace Test\Unit\JobHandler;

use InvalidArgumentException;
use mef\Job\JobHandler\PhpJobHandler;
use mef\Job\JobHandler\JobHandlerInterface;
use mef\Job\JobResult;

/**
 * @coversDefaultClass \mef\Job\JobHandler\PhpJobHandler
 */
class PhpJobHandlerTest extends \PHPUnit\Framework\TestCase
{
	private $workerFactoryMock;
	private $workerMock;

	public function setUp() : void
	{
		$this->workerFactoryMock = $this->getMockBuilder('\mef\Job\WorkerFactory\WorkerFactoryInterface')->getMock();
		$this->workerMock = $this->getMockBuilder('\mef\Job\Worker\WorkerInterface')->getMock();
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$jobHandler = new PhpJobHandler($this->workerFactoryMock, ['testName' => 'testName']);

		$this->assertInstanceOf(JobHandlerInterface::class, $jobHandler);

		return $jobHandler;
	}

	/**
	 * @covers ::startJob
	 * @depends testConstructor
	 */
	public function testStartJob(PhpJobHandler $jobHandler)
	{
		$this->workerMock->expects($this->once())->method('runJob')->willReturn(['test' => 'result']);
		$this->workerFactoryMock->expects($this->once())->method('createWorker')->willReturn($this->workerMock);

		$jobHandler = new PhpJobHandler($this->workerFactoryMock, ['testName' => 'testName']);

		$job = $this->getMockBuilder('\mef\Job\JobInfoInterface')->getMock();
		$job->expects($this->once())->method('getName')->willReturn('testName');

		$jobHandler->startJob($job);

		return $jobHandler;
	}

	/**
	 * @covers ::startJob
	 * @depends testConstructor
	 */
	public function testStartJobInvalidArgumentFailure(PhpJobHandler $jobHandler)
	{
		$this->expectException(InvalidArgumentException::class);
		$job = $this->getMockBuilder('\mef\Job\JobInfoInterface')->getMock();
		$job->expects($this->once())->method('getName')->willReturn('');

		$jobHandler->startJob($job);
	}

	/**
	 * @covers ::startJob
	 * @depends testConstructor
	 */
	public function testStartJobHandleAbstractResponseFailure(PhpJobHandler $jobHandler)
	{
		$abstractException = $this->getMockBuilder('\mef\Job\Exception\AbstractWorkerResponseException')->
			setMethods(['getResponse'])->
			disableOriginalConstructor()->
			getMock();

		$abstractException->expects($this->once())->method('getResponse')->willReturn([]);

		$this->workerMock->expects($this->once())->
			method('runJob')->
			will($this->throwException($abstractException));

		$this->workerFactoryMock->expects($this->once())->method('createWorker')->willReturn($this->workerMock);

		$jobHandler = new PhpJobHandler($this->workerFactoryMock, ['testName' => 'testName']);

		$job = $this->getMockBuilder('\mef\Job\JobInfoInterface')->getMock();
		$job->expects($this->once())->method('getName')->willReturn('testName');

		$jobHandler->startJob($job);

		return $jobHandler;
	}

	/**
	 * @covers ::startJob
	 * @depends testConstructor
	 */
	public function testStartJobHandleUnknownFailure(PhpJobHandler $jobHandler)
	{
		$this->workerMock->expects($this->once())->
			method('runJob')->
			will($this->throwException(new \Exception));

		$this->workerFactoryMock->expects($this->once())->method('createWorker')->willReturn($this->workerMock);

		$jobHandler = new PhpJobHandler($this->workerFactoryMock, ['testName' => 'testName']);

		$job = $this->getMockBuilder('\mef\Job\JobInfoInterface')->getMock();
		$job->expects($this->once())->method('getName')->willReturn('testName');

		$jobHandler->startJob($job);

		return $jobHandler;
	}

	/**
	 * @covers ::getRegisteredJobNames
	 * @depends testConstructor
	 */
	public function testGetRegisteredJobNames(PhpJobHandler $jobHandler)
	{
		$jobNames = $jobHandler->getRegisteredJobNames();

		$this->assertContains('testName', $jobNames);
	}

	/**
	 * @covers ::getResultsIterator
	 * @depends testStartJob
	 */
	public function testGetResultsIteratorSuccessCase(PhpJobHandler $jobHandler)
	{
		$jobResult = iterator_to_array($jobHandler->getResultsIterator(), false)[0];

		$this->assertInstanceOf(JobResult::class, $jobResult);
		$this->assertTrue($jobResult->isSuccessful());
		$this->assertIsArray($jobResult->getResponse());
		$this->assertArrayHasKey('test', $jobResult->getResponse());
		$this->assertIsFloat($jobResult->getRunTime());
	}

	/**
	 * @covers ::getResultsIterator
	 * @depends testStartJobHandleAbstractResponseFailure
	 */
	public function testGetResultsIteratorFailureCase1(PhpJobHandler $jobHandler)
	{
		$jobResult = iterator_to_array($jobHandler->getResultsIterator(), false)[0];

		$this->assertInstanceOf(JobResult::class, $jobResult);
		$this->assertFalse($jobResult->isSuccessful());
		$this->assertIsArray($jobResult->getResponse());
		$this->assertIsFloat($jobResult->getRunTime());
	}

	/**
	 * @covers ::getResultsIterator
	 * @depends testStartJobHandleUnknownFailure
	 */
	public function testGetResultsIteratorFailureCase2(PhpJobHandler $jobHandler)
	{
		$jobResult = iterator_to_array($jobHandler->getResultsIterator(), false)[0];

		$this->assertInstanceOf(JobResult::class, $jobResult);
		$this->assertFalse($jobResult->isSuccessful());
		$this->assertIsArray($jobResult->getResponse());
		$this->assertArrayHasKey('exception', $jobResult->getResponse());
		$this->assertArrayHasKey('message', $jobResult->getResponse());
		$this->assertIsFloat($jobResult->getRunTime());
	}
}
