<?php namespace Test\Unit\Exception;

use mef\Job\JobHandler\PhpJobHandlerResultsIterator;

use SplQueue;

/**
 * @coversDefaultClass \mef\Job\JobHandler\PhpJobHandlerResultsIterator
 */
class PhpJobHandlerResultsIteratorTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getIterator
	 */
	public function testIterator()
	{
		$queue = new SplQueue;
		$queue->enqueue(['key1', 'first']);
		$queue->enqueue(['key2', 'second']);

		$iterator = new PhpJobHandlerResultsIterator($queue);

		$data = [];
		foreach ($iterator as $key => $value)
		{
			$data[] = [$key, $value];
		}

		$this->assertEquals([['key1', 'first'], ['key2', 'second']], $data);
	}
}
