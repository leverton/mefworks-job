#!/usr/bin/env php
<?php namespace mef\Job;

if (!ini_get('date.timezone'))
{
	ini_set('date.timezone', 'UTC');
}

$autoloads = [
	__DIR__ . '/../vendor/autoload.php', // in bin folder
	__DIR__ . '/../../../autoload.php',  // installed vendor symlink
	__DIR__ . '/../autoload.php'         // installed vendor copy
];

foreach ($autoloads as $file)
{
	if (file_exists($file) === true)
	{
		define('JOB_SERVER_AUTOLOAD', $file);
		break;
	}
}

if (defined('JOB_SERVER_AUTOLOAD') === false)
{
	fwrite(STDERR, 'Unable to find composer\'s autoloader' . PHP_EOL);
	exit(1);
}

require JOB_SERVER_AUTOLOAD;

use Pimple\Container;
use mef\Job\Task\JobRunnerTask;

$options = getopt('', ['config:', 'job:', 'payload:']) +
	['config' => null, 'job' => null, 'payload' => null];

if ($options['config'] === null)
{
	die('Usage ' . $argv[0] . ' --config path/to/config' . PHP_EOL . PHP_EOL);
}

$config = require $options['config'];

$container = new Container;

if (isset($config['pimple']) === false)
{
	die('Pimple container must exist in config.' . PHP_EOL . PHP_EOL);
}

foreach ($config['pimple']['services'] as $key => $value)
{
	$container[$key] = $value;
}

foreach ($config['pimple']['factories'] as $key => $value)
{
	$container[$key] = $container->factory($value);
}

foreach ($config['pimple']['parameters'] as $key => $value)
{
	$container[$key] = (is_object($value) === true &&
		method_exists($value, '__invoke') === false) ?
		$container->protect($value) : $value;
}

if ($options['job'] === null)
{
	// run the never-ending job server handler
	$jobRunner = new JobRunnerTask($container['job-server'], $container['job-handler'], $container['logger']);
	$jobRunner->run();
}
else
{
	// add the job

	if ($options['payload'] === null)
	{
		$payload = [];
	}
	else
	{
		$payload = json_decode($options['payload'], true);
		if (is_array($payload) === false)
		{
			die('Payload must be a json encoded array.');
		}
	}

	$container['job-server']->enqueue($options['job'], $payload);
}