<?php

namespace mef\Job\JobHandler;

use IteratorAggregate;
use SplQueue;
use RuntimeException;
use Traversable;

/**
 * An iterator for PhpJobHandler::getResultsIterator().
 *
 * Iterates over the queue, removing the element as it does so.
 * It assumes that the queue contains an indexed array in the form of
 * [$jobId, $results].
 *
 * Only meant for use with the PhpJobHandler class.
 */
class PhpJobHandlerResultsIterator implements IteratorAggregate
{
    /**
     * Constructor
     *
     * @param \SplQueue $queue A queue with [$jobId, $result] pairs.
     */
    public function __construct(private SplQueue $queue)
    {
    }

    public function getIterator(): Traversable
    {
        while ($this->queue->isEmpty() === false) {
            [$jobId, $results] = $this->queue->dequeue();
            yield $jobId => $results;
        }
    }
}
