<?php

namespace mef\Job\JobHandler;

use Exception;
use InvalidArgumentException;
use Iterator;
use SplQueue;
use mef\Job\JobInfoInterface;
use mef\Job\Exception\WorkerResponseExceptionInterface;
use mef\Job\WorkerFactory\WorkerFactoryInterface;
use mef\Job\JobResult;
use Traversable;

/**
 * An implementation of JobHandlerInterface that runs PHP workers in-process,
 * synchronously, one at a time.
 *
 * It requires a WorkerFactoryInterface to create the workers from job names.
 * A new worker is created for every job.
 */
class PhpJobHandler implements JobHandlerInterface
{
    public const DEFAULT_EXPIRATION_AGE = 10;
    public const DEFAULT_MAXIMUM_JOBS = 1000;
    public const DEFAULT_CHECK_EXPIRATION_DELAY = 5;

    private const LAST_USED_INDEX = 0;
    private const WORKER_INDEX = 1;
    private const COUNT_INDEX = 2;

    /**
     * @var \SplQueue
     */
    private SplQueue $jobResults;

    /**
     * @var \mef\Job\JobHandler\PhpJobHandlerResultsIterator
     */
    private PhpJobHandlerResultsIterator $jobResultsIterator;

    /**
     * @var \mef\Job\WorkerFactory\WorkerFactoryInterface
     */
    private WorkerFactoryInterface $workerFactory;

    /**
     * @var array
     */
    private array $jobNames = [];

    /**
     * @var array
     */
    private array $jobNamesAsKeys = [];

    /**
     * @var array
     */
    private array $workers = [];

    /**
     * The number of seconds a worker can sit idle before being destroyed.
     *
     * @var int
     */
    private int $expirationAge;

    /**
     * The maximum number of jobs a worker can perform before being destroyed.
     *
     * @var int
     */
    private int $maximumJobs;

    /**
     * The number of seconds to wait before checking if any more workers should
     * be destroyed.
     *
     * @var int
     */
    private int $checkExpirationDelay;

    /**
     * The next time (UNIX timestamp) to check for expired workers.
     *
     * @var int
     */
    private int $nextExpirationCheck;

    /**
     * Constructor
     *
     * @param \mef\Job\WorkerFactory\WorkerFactoryInterface $workerFactory
     *              The factory to use to create the workers.
     * @param array $jobNames  A list of potential jobs to run.
     * @param array $options
     */
    public function __construct(WorkerFactoryInterface $workerFactory, array $jobNames = [], array $options = [])
    {
        $this->workerFactory = $workerFactory;
        $this->jobNames = array_values($jobNames);
        $this->jobNamesAsKeys = array_combine($this->jobNames, $this->jobNames);

        $this->jobResults = new SplQueue();
        $this->jobResultsIterator = new PhpJobHandlerResultsIterator($this->jobResults);

        $options += [
            'expiration_age' => self::DEFAULT_EXPIRATION_AGE,
            'maximum_jobs' => self::DEFAULT_MAXIMUM_JOBS,
            'check_expiration_delay' => self::DEFAULT_CHECK_EXPIRATION_DELAY
        ];

        $this->expirationAge = (int) $options['expiration_age'];
        $this->maximumJobs = (int) $options['maximum_jobs'];
        $this->checkExpirationDelay = (int) $options['check_expiration_delay'];
        $this->nextExpirationCheck = time() + $this->checkExpirationDelay;
    }

    /**
     * Start the job.
     *
     * It will run synchronously, thus the results will immediately be
     * available in the results iterator.
     *
     * @param \mef\Job\JobInfoInterface   $job  The job to run.
     */
    public function startJob(JobInfoInterface $job): void
    {
        $jobName = $job->getName();

        if (isset($this->jobNamesAsKeys[$jobName]) === false) {
            throw new InvalidArgumentException("No worker exists for $jobName");
        }

        $now = time();

        if ($this->nextExpirationCheck <= $now) {
            $this->killExpiredWorkers();
            $this->nextExpirationCheck = $now + $this->checkExpirationDelay;
        }

        if (isset($this->workers[$jobName]) === true) {
            $worker = $this->workers[$jobName][self::WORKER_INDEX];

            $this->workers[$jobName][self::LAST_USED_INDEX] = $now;
            $this->workers[$jobName][self::COUNT_INDEX]++;
        } else {
            $worker = $this->workerFactory->createWorker($jobName);

            $this->workers[$jobName] = [
                self::LAST_USED_INDEX => $now,
                self::WORKER_INDEX    => $worker,
                self::COUNT_INDEX     => 1
            ];
        }

        $startTime = microtime(true);

        try {
            $response = $worker->runJob($job) ?: [];
            $success = true;
        } catch (WorkerResponseExceptionInterface $e) {
            $response = $e->getResponse();
            $success = false;
        } catch (Exception $e) {
            $response = ['exception' => get_class($e), 'message' => $e->getMessage()];
            $success = false;
        }

        $this->jobResults->enqueue([$job->getId(), new JobResult($success, $response, microtime(true) - $startTime)]);
    }

    /**
     * Return the list of job names that will be handled.
     *
     * @return array
     */
    public function getRegisteredJobNames(): array
    {
        return $this->jobNames;
    }

    /**
     * Return the iterator that can be used to fetch results.
     *
     * @return \Iterator
     */
    public function getResultsIterator(): Traversable
    {
        return $this->jobResultsIterator;
    }

    private function killExpiredWorkers(): void
    {
        $expiredTime = time() - $this->expirationAge;
        $killedWorker = false;

        foreach ($this->workers as $jobName => list($lastUsed, $worker, $timesUsed)) {
            if ($lastUsed < $expiredTime || $timesUsed >= $this->maximumJobs) {
                unset($this->workers[$jobName]);
                $killedWorker = true;
            }
        }

        if ($killedWorker === true && gc_enabled() === true) {
            gc_collect_cycles();
        }
    }
}
