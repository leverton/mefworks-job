<?php

namespace mef\Job\JobHandler;

use mef\Job\JobInfoInterface;
use Traversable;

interface JobHandlerInterface
{
    /**
     * Start the requested job.
     *
     * The job may run asynchronously. The handler will be polled later to
     * retrieve the results.
     *
     * @param  \mef\Job\JobInfoInterface $job  the job to run
     */
    public function startJob(JobInfoInterface $job): void;

    /**
     * Return an iterator to loop through the results for jobs that have
     * finished running.
     *
     * The iterator is only valid while startJob() is not called.
     *
     * @return \Traversable
     */
    public function getResultsIterator(): Traversable;

    /**
     * Return a list of job names that the handler can run.
     *
     * @return array
     */
    public function getRegisteredJobNames(): array;
}
