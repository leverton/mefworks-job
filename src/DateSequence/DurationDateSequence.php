<?php

namespace mef\Job\DateSequence;

use DateTimeImmutable;
use DateInterval;
use Iterator;

/**
 * Generate a sequence of dates using the same format as DateInterval.
 */
class DurationDateSequence implements DateSequenceInterface
{
    /**
     * The DateInterval representation of the format.
     *
     * @var \DateInterval
     */
    private DateInterval $dateInterval;

    /**
     * Constructor
     *
     * @param string $format  The format as used by DateInterval
     */
    public function __construct(private string $format)
    {
        $this->dateInterval = new DateInterval($this->format);
    }

    /**
     * Return the sequence format.
     *
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * {@inheritdoc}
     */
    public function getNextDate(DateTimeImmutable $startDate): DateTimeImmutable
    {
        return $startDate->add($this->dateInterval);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator(DateTimeImmutable $startDate): Iterator
    {
        while (true) {
            yield $startDate;

            $startDate = $startDate->add($this->dateInterval);
        }

        // @codeCoverageIgnoreStart
    }
    // @codeCoverageIgnoreEnd

    /**
     * Return a serialized version of the object.
     *
     * Not to be called directly. For use with serialize($object).
     *
     * @return array
     */
    public function __serialize(): array
    {
        return [
            'format' => $this->format
        ];
    }

    /**
     * Initialize the object with the serialized data.
     *
     * Not to be called directly. For use with unserialize($serializedData).
     *
     * @param array $serialized  The serialized data.
     */
    public function __unserialize(array $serialized): void
    {
        $this->format = $serialized['format'];
        $this->dateInterval = new DateInterval($this->format);
    }
}
