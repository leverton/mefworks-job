<?php

namespace mef\Job\DateSequence;

use DateTimeImmutable;
use Iterator;

/**
 * Describes an interface to return the next date in a sequence.
 */
interface DateSequenceInterface
{
    /**
     * Get the next date after the supplied date.
     *
     * @param  \DateTimeImmutable $startDate
     *
     * @return \DateTimeImmutable  the next date
     */
    public function getNextDate(DateTimeImmutable $startDate): DateTimeImmutable;

    /**
     * Get a new iterator beginning with the specified date. Each successive
     * iteration will be based on the rules of the sequence.
     *
     * The iterator is not guaranteed to be rewindable. It may be infinite.
     *
     * Note that the $startDate may not necessarily be a "valid" date based on
     * the rules. e.g., If the sequence is "next Tuesday", supplying a Monday
     * to $startDate is still valid and will be returned as the first element
     * of the iteration.
     *
     * @param  \DateTimeImmutable $startDate
     *
     * @return \Iterator
     */
    public function getIterator(DateTimeImmutable $startDate): Iterator;
}
