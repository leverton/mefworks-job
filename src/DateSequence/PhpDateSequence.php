<?php

namespace mef\Job\DateSequence;

use DateTimeImmutable;
use DateTimeZone;
use InvalidArgumentException;
use Iterator;

/**
 * Generate a sequence of dates using the same format as DateTime::modify().
 */
class PhpDateSequence implements DateSequenceInterface
{
    /**
     * Constructor
     *
     * @param string             $format   The relative format as defined by
     *                                       DateTime::modify().
     * @param \DateTimeZone|null $timeZone The timezone to use. If the format
     *                                       contains a time portion, then this
     *                                       can be used to describe to which
     *                                       timezone it applies.
     */
    public function __construct(private string $format, private ?DateTimeZone $timeZone = null)
    {
    }

    /**
     * Return the sequence format.
     *
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * Return the timezone.
     *
     * @return \DateTimeZone|null
     */
    public function getTimeZone(): ?DateTimeZone
    {
        return $this->timeZone;
    }

    /**
     * Return a DateTimeImmutable that is in the same timezone as set by the
     * $timeZone instance variable.
     *
     * If there is no timezone or $date is already in the proper timezone, it
     * is returned unmodified.
     *
     * @param  \DateTimeImmutable $date   The date to convert.
     * @return \DateTimeImmutable         The same date using this object's
     *                                      timezone.
     */
    private function getDateAdjustedForTimeZone(DateTimeImmutable $date): DateTimeImmutable
    {
        if ($this->timeZone !== null && $date->getTimeZone()->getName() !== $this->timeZone->getName()) {
            $date = $date->setTimeZone($this->timeZone);
        }

        return $date;
    }

    /**
     * {@inheritdoc}
     */
    public function getNextDate(DateTimeImmutable $startDate): DateTimeImmutable
    {
        return $this->getDateAdjustedForTimeZone($startDate)->modify($this->format);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator(DateTimeImmutable $startDate): Iterator
    {
        $date = $this->getDateAdjustedForTimeZone($startDate);

        while (true) {
            yield $date;

            $date = $date->modify($this->format);
        }

        // @codeCoverageIgnoreStart
    }
    // @codeCoverageIgnoreEnd

    /**
     * Return a serialized version of the object.
     *
     * Not to be called directly. For use with serialize($object).
     *
     * @return array
     */
    public function __serialize(): array
    {
        return [
            'format' => $this->format,
            'tz' => $this->timeZone?->getName()
        ];
    }

    /**
     * Initialize the object with the serialized data.
     *
     * Not to be called directly. For use with unserialize($serializedData).
     *
     * @param string $serialized  The serialized data.
     */
    public function __unserialize(array $data): void
    {
        $this->format = $data['format'];
        $this->timeZone = isset($data['tz']) ? new DateTimeZone($data['tz']) : null;
    }
}
