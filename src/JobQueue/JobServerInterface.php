<?php

namespace mef\Job\JobQueue;

use DateTimeImmutable;
use mef\Job\DateSequence\DateSequenceInterface;
use mef\Job\JobInterface;
use mef\Job\JobStore\JobStoreInterface;

/**
 * An extension to JobQueueInterface that adds the ability to schedule jobs to
 * run.
 *
 * Depends upon a JobStoreInterface object to persist that information.
 */
interface JobServerInterface extends JobQueueInterface
{
    /**
     * Return the JobStore used by this server.
     *
     * @return \mef\Job\JobStore\JobStoreInterface
     */
    public function getJobStore(): JobStoreInterface;

    /**
     * Schedule a job to run at the given time, optionally repeating.
     *
     * @param  string             $name          The name of the job.
     * @param  array              $payload       The payload of the job.
     * @param  \DateTimeImmutable $startDateTime The time the job should start.
     * @param  \mef\Job\DateSequence\DateSequenceInterface|null $repeatSequence
     *    The repeat sequence for recurring jobs.
     *
     * @return string  The job's id.
     */
    public function scheduleJob(string $name, array $payload, DateTimeImmutable $startDateTime, DateSequenceInterface $repeatSequence = null): string;

    /**
     * Fetch the given job.
     *
     * @param  string $jobId  The id of the job.
     *
     * @return \mef\Job\JobInterface  The requested job.
     */
    public function fetchJob(string $jobId): JobInterface;
}
