<?php

namespace mef\Job\JobQueue;

/**
 * An interface for queueing jobs.
 *
 * For scheduling, see the JobServerInterface.
 */
interface JobQueueInterface
{
    /**
     * Enqueue a job for processing.
     *
     * @param  string                 $name
     * @param  array                  $payload
     *
     * @return string  The job's id
     */
    public function enqueue(string $name, array $payload = []): string;

    /**
     * Dequeue (cancel) a job.
     *
     * @param  string $jobId
     */
    public function dequeue(string $jobId): void;

    /**
     * Return true if the job is currently queued.
     *
     * @param  string $jobId
     *
     * @return boolean
     */
    public function isQueued(string $jobId): bool;
}
