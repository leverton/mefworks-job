<?php

namespace mef\Job\JobQueue;

use Closure;
use DateTimeImmutable;
use Exception;
use mef\Job\JobInfo;
use mef\Job\JobSchedule;
use mef\Job\JobState;
use mef\Job\JobStore\JobStoreInterface;
use mef\Job\DateSequence\DateSequenceInterface;
use mef\Job\JobInterface;

/**
 * An implementation of JobServerInterface.
 *
 * All jobs are placed in the JobStore. The jobs are meant to be processed by
 * the JobRunnerTask via the CLI bin/job-server.php script.
 */
class JobServer implements JobServerInterface
{
    /**
     * A callback that returns a unique id for each new job.
     *
     * @var Closure
     */
    private Closure $idGenerator;

    /**
     * Constructor
     *
     * @param \mef\Job\JobStore\JobStoreInterface  $jobStore
     * @param Closure $idGenerator   must return a unique id as a string.
     *                                Defaults to the 'uniqid' function.
     */
    public function __construct(private JobStoreInterface $jobStore, Closure $idGenerator = null)
    {
        $this->idGenerator = $idGenerator ?: uniqid(...);
    }

    /**
     * {@inheritdoc}
     */
    public function getJobStore(): JobStoreInterface
    {
        return $this->jobStore;
    }

    /**
     * {@inheritdoc}
     */
    public function enqueue(string $name, array $payload = []): string
    {
        return $this->scheduleJob($name, $payload, new DateTimeImmutable('UTC'));
    }

    /**
     * {@inheritdoc}
     */
    public function dequeue(string $jobId): void
    {
        $this->jobStore->cancelJob($jobId);
    }

    /**
     * {@inheritdoc}
     */
    public function isQueued(string $jobId): bool
    {
        try {
            $job = $this->jobStore->getJob($jobId);
            $state = $job->getStatus()->getState();
            $isQueued = ($state === JobState::QUEUED || $state === JobState::RUNNING);
        } catch (Exception $e) {
            $isQueued = false;
        }

        return $isQueued;
    }

    /**
     * {@inheritdoc}
     */
    public function scheduleJob(string $name, array $payload, DateTimeImmutable $startDateTime, DateSequenceInterface $repeatSequence = null): string
    {
        $info = new JobInfo(($this->idGenerator)(), $name, $payload);
        $schedule = new JobSchedule($startDateTime, $repeatSequence);

        $this->jobStore->addJob($info, $schedule);

        return $info->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function fetchJob(string $jobId): JobInterface
    {
        return $this->jobStore->getJob($jobId);
    }
}
