<?php

namespace mef\Job;

/**
 * An immutable JobInfoInterface type.
 */
class JobInfo implements JobInfoInterface
{
    /**
     * Constructor
     *
     * @param string $id      The unique id of the job.
     * @param string $name    The name of the job.
     * @param array  $payload The payload (metadata) for the job.
     */
    public function __construct(private string $id, private string $name, private array $payload = [])
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getPayload(): array
    {
        return $this->payload;
    }
}
