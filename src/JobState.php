<?php

namespace mef\Job;

use InvalidArgumentException;

/**
 * An abstract class to hold the enumeration state.
 */
enum JobState: string
{
    case QUEUED = 'queued';
    case RUNNING = 'running';
    case DONE = 'done';
}
