<?php

namespace mef\Job;

/**
 * A read-only interface to get the job's result.
 */
interface JobResultInterface
{
    /**
     * Return true if the job was successful.
     *
     * @return bool
     */
    public function isSuccessful(): bool;

    /**
     * Return the response.
     *
     * @return array
     */
    public function getResponse(): array;

    /**
     * Return the run time.
     *
     * @return float
     */
    public function getRunTime(): float;
}
