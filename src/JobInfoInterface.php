<?php

namespace mef\Job;

/**
 * A read-only interface get get a job's info (id, name, payload).
 */
interface JobInfoInterface
{
    /**
     * The unique id for the job.
     *
     * @return string
     */
    public function getId(): string;

    /**
     * The name of the job.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * The job's payload (metadata).
     *
     * @return array
     */
    public function getPayload(): array;
}
