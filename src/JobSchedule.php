<?php

namespace mef\Job;

use DateTimeImmutable;
use mef\Job\DateSequence\DateSequenceInterface;

/**
 * An immutable JobScheduleInterface type.
 */
class JobSchedule implements JobScheduleInterface
{
    /**
     * Constructor
     *
     * @param \DateTimeImmutable $scheduledDateTime The date the job should
     *                                              start.
     *
     * @param \mef\Job\DateSequence\DateSequenceInterface|null $repeatSequence
     *    The repeat syntax for recurring jobs.
     */
    public function __construct(private DateTimeImmutable $scheduledDateTime, private ?DateSequenceInterface $repeatSequence = null)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getScheduledDateTime(): DateTimeImmutable
    {
        return $this->scheduledDateTime;
    }

    /**
     * {@inheritdoc}
     */
    public function getRepeatSequence(): ?DateSequenceInterface
    {
        return $this->repeatSequence;
    }
}
