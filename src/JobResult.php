<?php

namespace mef\Job;

/**
 * An immutable JobResultInterface type.
 */
class JobResult implements JobResultInterface
{
    /**
     * @param bool   $isSuccessful
     * @param array  $response
     * @param float  $runTime
     */
    public function __construct(private bool $isSuccessful, private array $response = [], private float $runTime = 0.0)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function isSuccessful(): bool
    {
        return $this->isSuccessful;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse(): array
    {
        return $this->response;
    }

    /**
     * {@inheritdoc}
     */
    public function getRunTime(): float
    {
        return $this->runTime;
    }
}
