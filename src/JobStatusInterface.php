<?php

namespace mef\Job;

use DateTimeImmutable;

/**
 * A read-only interface to get the job's status.
 */
interface JobStatusInterface
{
    /**
     * Return the state as one of the following enum values:
     *
     * * \mef\Job\JobState::QUEUED
     * * \mef\Job\JobState::RUNNING
     * * \mef\Job\JobState::DONE
     *
     * @return JobState
     */
    public function getState(): JobState;

    /**
     * Return the date the job started.
     *
     * @return \DateTimeImmutable|null
     */
    public function getStartedDateTime(): ?DateTimeImmutable;
}
