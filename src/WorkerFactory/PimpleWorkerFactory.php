<?php

namespace mef\Job\WorkerFactory;

use Closure;
use ReflectionClass;
use ReflectionException;
use RuntimeException;
use Pimple\Container;
use mef\Job\Worker\PimpleWorkerInterface;

/**
 * Creates a worker that implements PimpleWorkerInterface by passing it the
 * container associated with the factory.
 */
class PimpleWorkerFactory implements WorkerFactoryInterface
{
    /**
     * Constructor
     *
     * @param \Pimple\Container $container  The Pimple container to pass to the
     *                                      worker's fromPimpleContainer
     *                                      method.
     * @param Closure          $inflector   A callable that returns a class
     *                                      name from the supplied job name.
     */
    public function __construct(private Container $container, private Closure $inflector)
    {
    }

    /**
     * Return worker.
     *
     * @param  string $jobName  The name of the job.
     *
     * @return \mef\Job\Worker\PimpleWorkerInterface
     */
    public function createWorker(string $jobName): PimpleWorkerInterface
    {
        $className = ($this->inflector)($jobName);

        try {
            $reflector = new ReflectionClass($className);
        } catch (ReflectionException $e) {
            throw new RuntimeException($className . ' does not exist.');
        }

        if ($reflector->implementsInterface(PimpleWorkerInterface::class) === false) {
            throw new RuntimeException($className . ' does not implement ' . PimpleWorkerInterface::class);
        }

        return $reflector->getMethod('fromPimpleContainer')->invoke(null, $this->container);
    }
}
