<?php

namespace mef\Job\WorkerFactory;

use mef\Job\Worker\WorkerInterface;

/**
 * A WorkerFactory creates a worker for the given job name.
 */
interface WorkerFactoryInterface
{
    /**
     * Create a worker that is able to run jobs with the given name.
     *
     * @param  string $jobName  The name of the job.
     *
     * @return \mef\Job\Worker\WorkerInterface
     */
    public function createWorker(string $jobName): WorkerInterface;
}
