<?php

namespace mef\Job\Worker;

use Pimple\Container;
use mef\Job\JobInfoInterface;
use Psr\Log\LoggerInterface;

/**
 * A sample worker that simply sends a "Hello" message to the configured
 * logger.
 */
class HelloWorker implements PimpleWorkerInterface
{
    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(private LoggerInterface $logger)
    {
        $this->logger->debug('{0} created. {1}', [__CLASS__, spl_object_hash($this)]);
    }

    /**
     * Destructor
     */
    public function __destruct()
    {
        $this->logger->debug('{0} destroyed. {1}', [__CLASS__, spl_object_hash($this)]);
    }

    /**
     * Configure a new HelloWorker from the Pimple container.
     *
     * @param  \Pimple\Container  $container  Must contain:
     *   * logger: \Psr\Log\LoggerInterface
     *
     * @return \mef\Job\Worker\HelloWorker
     */
    public static function fromPimpleContainer(Container $container): HelloWorker
    {
        return new self($container['logger']);
    }

    /**
     * Log an informational message indicating that we received the job.
     *
     * @param  \mef\Job\JobInfoInterface $job
     */
    public function runJob(JobInfoInterface $job): ?array
    {
        $this->logger->info('Hello, from job {0}/{1}', [$job->getName(), $job->getId()]);

        return null;
    }
}
