<?php

namespace mef\Job\Worker;

use mef\Job\JobInfoInterface;

/**
 * A worker runs a job and returns the result as an array.
 *
 * It is only passed the information needed to run the job. It has no concept
 * of a job queue or server. Any such services must be injected into the worker
 * at the time it is created.
 */
interface WorkerInterface
{
    /**
     * Run the job.
     *
     * @param  \mef\Job\JobInfoInterface $jobInfo  Contains the job's name, id,
     *                                             and payload.
     *
     * @return array|null  Results must be returned by an associative array. If
     *   there are no results to report, return nothing (null).
     */
    public function runJob(JobInfoInterface $jobInfo): ?array;
}
