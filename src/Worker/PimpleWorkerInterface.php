<?php

namespace mef\Job\Worker;

use Pimple\Container;

/**
 * An extension of WorkerInterface. Workers that can instantiate themselves
 * from a pimple service should implement this interface.
 *
 * Meant to be used with a PimpleWorkerFactory.
 */
interface PimpleWorkerInterface extends WorkerInterface
{
    /**
     * Create and return a Worker from the given container.
     *
     * @param  \Pimple\Container  $container The pimple container from which to
     *                                       obtain the services.
     *
     * @return \mef\Job\Worker\PimpleWorkerInterface
     */
    public static function fromPimpleContainer(Container $container): PimpleWorkerInterface;
}
