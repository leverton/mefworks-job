<?php

namespace mef\Job;

use DateTimeImmutable;
use mef\Job\DateSequence\DateSequenceInterface;

/**
 * A read-only interface to get the job's schedule.
 */
interface JobScheduleInterface
{
    /**
     * Return the scheduled date for the job.
     *
     * @return \DateTimeImmutable
     */
    public function getScheduledDateTime(): DateTimeImmutable;

    /**
     * Return the repeat sequence for the job.
     *
     * @return \mef\Job\DateSequence\DateSequenceInterface
     */
    public function getRepeatSequence(): ?DateSequenceInterface;
}
