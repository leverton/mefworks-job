<?php

namespace mef\Job;

use InvalidArgumentException;

/**
 * An immutable JobInterface data type.
 */
class Job implements JobInterface
{
    /**
     * Constructor
     *
     * @param \mef\Job\JobInfoInterface        $info
     * @param \mef\Job\JobStatusInterface      $status
     * @param \mef\Job\JobScheduleInterface    $schedule
     * @param \mef\Job\JobResultInterface|null $result
     */
    public function __construct(private JobInfoInterface $info, private JobStatusInterface $status, private JobScheduleInterface $schedule, private ?JobResultInterface $result = null)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getInfo(): JobInfoInterface
    {
        return $this->info;
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus(): JobStatusInterface
    {
        return $this->status;
    }

    /**
     * {@inheritdoc}
     */
    public function getSchedule(): JobScheduleInterface
    {
        return $this->schedule;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): ?JobResultInterface
    {
        return $this->result;
    }
}
