<?php

namespace mef\Job;

/**
 * A read-only interface for jobs.
 */
interface JobInterface
{
    /**
     * @return \mef\Job\JobInfoInterface
     */
    public function getInfo(): JobInfoInterface;

    /**
     * @return \mef\Job\JobStatusInterface
     */
    public function getStatus(): JobStatusInterface;

    /**
     * @return \mef\Job\JobResultInterface|null
     */
    public function getResult(): ?JobResultInterface;

    /**
     * @return \mef\Job\JobScheduleInterface
     */
    public function getSchedule(): JobScheduleInterface;
}
