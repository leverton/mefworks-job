<?php

namespace mef\Job;

use DateTimeImmutable;

/**
 * An immutable JobStatusInterface type.
 */
class JobStatus implements JobStatusInterface
{
    /**
     * Constructor
     *
     * @param JobState                 $state
     * @param \DateTimeImmutable|null  $startedDateTime  The date the job
     *                                                   started.
     */
    public function __construct(private JobState $state, private ?DateTimeImmutable $startedDateTime = null)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getState(): JobState
    {
        return $this->state;
    }

    /**
     * {@inheritdoc}
     */
    public function getStartedDateTime(): ?DateTimeImmutable
    {
        return $this->startedDateTime;
    }
}
