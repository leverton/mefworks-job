<?php

namespace mef\Job\Exception;

use Exception as PhpException;

/**
 * Extend this class to create your own worker response exceptions.
 *
 * Set $this->response['your key'] in your constructor after calling the parent
 * constructor. See RetryException for an example of this.
 *
 * Alternatively, you may simply implement the WorkerResponseExceptionInterface
 * in your own exceptions.
 */
abstract class AbstractWorkerResponseException extends Exception implements WorkerResponseExceptionInterface
{
    /**
     * @var array  The response that will be logged in the JobStore
     */
    protected array $response = [];

    /**
     * Constructor
     *
     * @param string     $message   The exception message
     * @param int        $code      The exception code
     * @param \Exception $previous  The previous exception used for the exception chaining.
     */
    public function __construct(string $message = '', int $code = 0, PhpException $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->response['exception'] = get_class($this);
        $this->response['message'] = $message;
    }

    /**
     * Return the response that will be serialized into a string and stored in
     * the JobStore.
     *
     * @return array  Serializable response data.
     */
    public function getResponse(): array
    {
        return $this->response;
    }
}
