<?php

namespace mef\Job\Exception;

use DateTimeImmutable;

/**
 * Thrown by Workers, representing the case where there was a temporary error
 * and that if the task is retried at the specified date, it is likely to
 * succeed.
 */
class RetryException extends AbstractWorkerResponseException
{
    /**
     * Constructor
     *
     * @param \DateTimeImmutable $rescheduleDate  The time when the task should be able to run
     * @param string $message                     The error message
     */
    public function __construct(private DateTimeImmutable $rescheduleDate, string $message)
    {
        parent::__construct($message);

        $this->response['reschedule'] = $rescheduleDate->format('c');
    }

    /**
     * Return the reschedule date.
     *
     * @return \DateTimeImmutable  Date to rerun the job
     */
    public function getRescheduleDate(): DateTimeImmutable
    {
        return $this->rescheduleDate;
    }
}
