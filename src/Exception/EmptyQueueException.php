<?php

namespace mef\Job\Exception;

use Exception as PhpException;

/**
 * An exception that signifies that an operation was requested on an empty
 * JobQueue.
 */
class EmptyQueueException extends Exception
{
    /**
     * Constructor
     *
     * @param string     $message   The exception message
     * @param int        $code      The exception code
     * @param \Exception $previous  The previous exception used for the exception chaining.
     */
    public function __construct(string $message = 'The job queue is empty', int $code = 0, PhpException $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
