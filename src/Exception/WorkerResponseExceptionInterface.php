<?php

namespace mef\Job\Exception;

/**
 * Exceptions may implement this interface to signify that the getResponse()
 * method should be used when serializing the information into the JobStore.
 *
 * The AbstractWorkerResponseException implements this interface and can be
 * used as the basis of your own exceptions.
 */
interface WorkerResponseExceptionInterface
{
    /**
     * Return the response that will be serialized into a string and stored in
     * the JobStore.
     *
     * @return array  Serializable response data.
     */
    public function getResponse(): array;
}
