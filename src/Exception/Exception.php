<?php

namespace mef\Job\Exception;

/**
 * An empty class that all mef\Job exceptions extend from.
 */
class Exception extends \Exception
{

}
