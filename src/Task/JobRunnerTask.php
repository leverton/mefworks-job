<?php

namespace mef\Job\Task;

use DateTimeImmutable;
use Exception;
use mef\Job\JobInterface;
use mef\Job\JobScheduleInterface;
use mef\Job\JobQueue\JobServerInterface;
use mef\Job\JobHandler\JobHandlerInterface;
use mef\Job\Exception\Exception as JobException;
use mef\Job\Exception\RetryException;
use mef\Job\Exception\EmptyQueueException;
use mef\Job\JobStore\JobStoreInterface;
use Psr\Log\LoggerInterface;

/**
 * A CLI application that monitors a JobServer for new jobs and then delegates
 * the work out to a JobHandler. The JobHandler is responsible for creating a
 * worker and reporting the results.
 *
 * The bin/job-server.php script is provided to bootstrap an instance of the
 * application.
 */
class JobRunnerTask
{
    /**
     * @var \mef\Job\JobStore\JobStoreInterface
     */
    private JobStoreInterface $jobStore;

    /**
     * @var boolean
     */
    private bool $wantsToQuit = false;

    /**
     * Constructor
     *
     * @param \mef\Job\JobQueue\JobServerInterface    $jobServer
     * @param \mef\Job\JobHandler\JobHandlerInterface $handler
     * @param \Psr\Log\LoggerInterface                $logger
     */
    public function __construct(private JobServerInterface $jobServer, private JobHandlerInterface $handler, private LoggerInterface $logger)
    {
        $this->jobStore = $jobServer->getJobStore();
    }

    /**
     * Main entry point of the task.
     *
     * Will run until SIGINT is received.
     */
    public function run(): void
    {
        pcntl_signal(\SIGINT, function ($signo) {
            $this->wantsToQuit = true;
            $this->logger->info('Ctrl-C received');
        });

        $jobs = $this->handler->getRegisteredJobNames();

        if ($jobs === []) {
            $errorMessage = 'There are no registered jobs. Nothing to do.';
            $this->logger->error($errorMessage);
            throw new JobException($errorMessage);
        }

        try {
            while ($this->wantsToQuit === false) {
                if ($this->tryNextJob($jobs) === false) {
                    usleep(1000000 * 0.10);
                }
                $this->processResults();
                pcntl_signal_dispatch();
            }
        } catch (Exception $e) {
            $this->logger->critical('Unhandled exception: {exception}', ['exception' => $e]);
            throw $e;
        }
    }

    /**
     * Try to start the next job from the given job names.
     *
     * @param array $jobNames  An array of job names.
     */
    private function tryNextJob(array $jobNames): bool
    {
        try {
            $jobId = $this->jobStore->startNextJob($jobNames);
            $job = $this->jobStore->getJob($jobId);

            $this->logger->info('Starting job {name}/{id}', [
                'id' => $jobId,
                'name' => $job->getInfo()->getName()
            ]);

            $this->handler->startJob($job->getInfo());

            return true;
        } catch (EmptyQueueException $e) {
            return false;
        }
    }

    /**
     * Process any jobs that have finished.
     */
    private function processResults(): void
    {
        foreach ($this->handler->getResultsIterator() as $jobId => $result) {
            $this->jobStore->finishJob($jobId, $result);
            $job = $this->jobStore->getJob($jobId);

            $this->logger->info('Finished job {id} in {runTime} seconds: {success} {response}', [
                'id' => $jobId,
                'runTime' => $result->getRunTime(),
                'success' => $result->isSuccessful() ? 'success' : 'failure',
                'response' => $result->getResponse()
            ]);

            $this->scheduleNextJob($job);
        }
    }

    /**
     * Schedule the next job if it is repeating or was asked to be retried.
     *
     * @param  \mef\Job\JobInterface $job
     */
    private function scheduleNextJob(JobInterface $job): void
    {
        $info = $job->getInfo();
        $jobId = $info->getId();
        $schedule = $job->getSchedule();
        $result = $job->getResult();

        if ($result->isSuccessful() === false) {
            $response = $result->getResponse();
            if (isset($response['exception']) === true && $response['exception'] === RetryException::class) {
                $date = new DateTimeImmutable($response['reschedule']);
                $rescheduledJobId = $this->jobServer->scheduleJob($info->getName(), $info->getPayload(), $date);

                $this->logger->info('Retrying job {0} at {1} as {2}', [$jobId, $date, $rescheduledJobId]);
            }
        }

        $nextDate = $this->calculateNextDate($schedule);

        if ($nextDate !== null) {
            if ($nextDate < new DateTimeImmutable()) {
                $this->logger->error('Job {0} asked to schedule the next iteration in the past ({1})', [
                    $jobId, $nextDate
                ]);
            } else {
                $nextJobId = $this->jobServer->scheduleJob($info->getName(), $info->getPayload(), $nextDate, $schedule->getRepeatSequence());
                $this->logger->info('Scheduled next instance of job {0} at {1} as {2}', [$jobId, $nextDate, $nextJobId]);
            }
        }
    }

    /**
     * Calculate the next time the job should run.
     *
     * @param  \mef\Job\JobScheduleInterface $schedule
     *
     * @return null|\DateTimeImmutable
     */
    private function calculateNextDate(JobScheduleInterface $schedule): ?DateTimeImmutable
    {
        $sequence = $schedule->getRepeatSequence();

        return $sequence !== null ? $sequence->getNextDate($schedule->getScheduledDateTime()) : null;
    }
}
