<?php

namespace mef\Job\JobStore;

use mef\Job\JobInfoInterface;
use mef\Job\JobInterface;
use mef\Job\JobScheduleInterface;
use mef\Job\JobResultInterface;

/**
 * The JobStoreInterface defines an API to access job data as needed by
 * objects implementing JobServerInterface.
 */
interface JobStoreInterface
{
    /**
     * Add the given (new) job to the datastore for the requested schedule.
     *
     * @param \mef\Job\JobInfoInterface     $info
     * @param \mef\Job\JobScheduleInterface $schedule
     */
    public function addJob(JobInfoInterface $info, JobScheduleInterface $schedule): void;

    /**
     * Cancel the job.
     *
     * The job must exist and be in 'queued' status. Otherwise, an exception
     * will be thrown.
     *
     * @param string $jobId  The id of the job to cancel.
     */
    public function cancelJob(string $jobId): void;

    /**
     * Mark the job as finished using the supplied results.
     *
     * @param string $jobId  The id of the job to finish.
     * @param \mef\Job\JobResultInterface $result  The results of the job.
     */
    public function finishJob(string $jobId, JobResultInterface $result): void;

    /**
     * Fetch the job from the datastore.
     *
     * If it does not exist, an exception will be thrown. Note that a datastore
     * may delete finished jobs after a certain time period.
     *
     * @param string $jobId  The id of the job to fetch.
     *
     * @return \mef\Job\JobInterface  The job with the given id.
     */
    public function getJob(string $jobId): JobInterface;

    /**
     * Mark the next job in the queue as 'running'.
     *
     * Only jobs matching one of the entries in $jobNames will be considered.
     * If no jobs are found, an exception is thrown.
     *
     * @param  array  $jobNames [description]
     *
     * @return string  The id of the job.
     *
     * @throws \mef\Job\Exception\EmptyQueueException
     */
    public function startNextJob(array $jobNames): string;
}
