<?php

namespace mef\Job\JobStore;

use DateTimeImmutable;
use DateTimeZone;
use Exception;
use mef\Db\Driver\DriverInterface;
use mef\Job\Job;
use mef\Job\JobResult;
use mef\Job\JobResultInterface;
use mef\Job\JobInfo;
use mef\Job\JobInfoInterface;
use mef\Job\JobSchedule;
use mef\Job\JobScheduleInterface;
use mef\Job\JobState;
use mef\Job\JobStatus;
use mef\Job\Exception\EmptyQueueException;
use mef\Job\JobInterface;

class DatabaseJobStore implements JobStoreInterface
{
    /**
     * Constructor
     *
     * @param \mef\Db\Driver\DriverInterface $db
     */
    public function __construct(private DriverInterface $db)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function addJob(JobInfoInterface $info, jobScheduleInterface $schedule): void
    {
        $this->db->prepare('
			INSERT INTO job (uuid,name,status,payload,recur,created_dt,scheduled_dt)
			VALUES (:uuid,:name,\'queued\',:payload,:recur,:created_dt,:scheduled_dt)', [
            ':uuid' => $info->getId(),
            ':name' => $info->getName(),
            ':payload' => json_encode($info->getPayload()),
            ':recur' => serialize($schedule->getRepeatSequence()),
            ':created_dt' => gmdate('Y-m-d H:i:s'),
            ':scheduled_dt' => $this->getUTCTime($schedule->getScheduledDateTime())->format('Y-m-d H:i:s')
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function cancelJob(string $jobId): void
    {
        // @todo: create specific exceptions
        $this->db->startTransaction();
        try {
            $job = $this->db->prepare('SELECT * FROM job WHERE uuid=? FOR UPDATE', [$jobId])->query()->fetchRow();

            if ($job === []) {
                throw new Exception('Job not found');
            }

            if ($job['status'] !== 'queued') {
                throw new Exception('Job is not queued');
            }

            $this->db->prepare('DELETE FROM job WHERE id=?', [$job['id']])->execute();
            $this->db->commit();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw $e;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function startNextJob(array $jobNames): string
    {
        // @todo: create specific exceptions
        $this->db->startTransaction();
        try {
            $job = [];

            foreach ($jobNames as $jobName) {
                $job = $this->db->prepare('
					SELECT id,uuid
					FROM job
					WHERE name=:name AND status=\'queued\' AND scheduled_dt <= :now
					ORDER BY created_dt
					LIMIT 1
					FOR UPDATE', [':name' => $jobName, ':now' => gmdate('Y-m-d H:i:s')])->query()->fetchRow();

                if ($job !== []) {
                    break;
                }
            }

            if ($job === []) {
                throw new EmptyQueueException();
            }

            $this->db->prepare('
				UPDATE job
				SET status=\'running\',started_dt=:now
				WHERE id=:id', [
                    ':id' => $job['id'],
                    ':now' => gmdate('Y-m-d H:i:s')
                ])->execute();

            $this->db->commit();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw $e;
        }

        return $job['uuid'];
    }

    /**
     * {@inheritdoc}
     */
    public function finishJob(string $jobId, JobResultInterface $result): void
    {
        // @todo: create specific exceptions
        $this->db->startTransaction();
        try {
            $job = $this->db->prepare('SELECT * FROM job WHERE uuid=? FOR UPDATE', [$jobId])->query()->fetchRow();

            if ($job === []) {
                throw new Exception('Job not found');
            }

            if ($job['status'] !== 'running') {
                throw new Exception('Job is not running');
            }

            $this->db->prepare('
				UPDATE job
				SET status=\'done\',is_successful=:is_successful,response=:response,run_time=:run_time
				WHERE id=:id', [
                    ':id' => $job['id'],
                    ':is_successful' => $result->isSuccessful() ? 1 : 0,
                    ':response' => json_encode($result->getResponse()),
                    ':run_time' => $result->getRunTime()
                ])->execute();
            $this->db->commit();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw $e;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getJob(string $jobId): JobInterface
    {
        // @todo: create specific exceptions

        $data = $this->db->prepare('SELECT * FROM job WHERE uuid=? LIMIT 1', [$jobId])->query()->fetchRow();

        if ($data === []) {
            throw new Exception('Job does not exist');
        }

        if ($data['status'] === 'done') {
            $result = new JobResult($data['is_successful'] == 1, json_decode($data['response'], true) ?: [], (float) $data['run_time']);
        } else {
            $result = null;
        }

        $info = new JobInfo($data['uuid'], $data['name'], json_decode($data['payload'], true) ?: []);
        $state = JobState::from($data['status']);
        $status = new JobStatus($state);
        $schedule = new JobSchedule(new DateTimeImmutable($data['scheduled_dt'] . ' UTC'), $data['recur'] ? unserialize($data['recur']) : null);

        return new Job($info, $status, $schedule, $result);
    }

    /**
     * Returns an instance of DateTimeImmutable that is in UTC.
     *
     * If the passed date is already UTC, then the same object is returned.
     * Otherwise, a DateTimeImmutable copy is returned.
     *
     * @param  \DateTimeImmutable $dt
     *
     * @return \DateTimeImmutable
     */
    private function getUTCTime(DateTimeImmutable $dt): DateTimeImmutable
    {
        if ($dt->getTimeZone()->getName() !== 'UTC') {
            $dt = $dt->setTimeZone(new DateTimeZone('UTC'));
        }

        return $dt;
    }
}
