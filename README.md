mef\Job - Job Scheduler
=======================

[![Total Downloads](https://poser.pugx.org/mefworks/job/downloads.png)](https://packagist.org/packages/mefworks/job)
[![Latest Stable Version](https://poser.pugx.org/mefworks/job/v/stable.png)](https://packagist.org/packages/mefworks/job)

mef\Job provides interfaces for job queues and workers and a job server
implementation to process jobs from backend workers.

Examples
--------

*Enqueue a job*

	<?php
	use mef\Job\JobQueue\JobServer;
	use mef\Job\JobStore\DatabaseJobStore;

	$jobServer = new JobServer(new DatabaseJobStore($db));

	// without a payload
	$jobServer->enqueue('job name');

	// with a payload
	$jobServer->enqueue('job name', ['param1' => 'val1']);

*Schedule a job*

	<?php
	use mef\Job\JobQueue\JobServer;
	use mef\Job\JobStore\DatabaseJobStore;

	$jobServer = new JobServer(new DatabaseJobStore($db));
	$jobServer->scheduleJob('job name', ['param1' => 'val1'], new DateTimeImmutable('+5 minutes'));

*Schedule a repeating job*

	<?php
	use mef\Job\JobQueue\JobServer;
	use mef\Job\JobStore\DatabaseJobStore;

	$jobServer = new JobServer(new DatabaseJobStore($db));
	$jobServer->scheduleJob('job name', ['param1' => 'val1'], new DateTimeImmutable, 'next Tuesday');

*A sample worker*

	<?php
	use mef\Job\JobInfoInterface;
	use mef\Job\Worker\WorkerInterface;

	class CopyFileWorker implements WorkerInterface
	{
		public function runJob(JobInfoInterface $jobInfo)
		{
			$payload = $jobInfo->getPayload();

			if (copy($payload['src'], $payload['dest']) !== true)
			{
				throw new Exception('Unable to copy file');
			}
		}
	}

Running the job server
----------------------

A single-threaded job server that runs PHP workers in-process is included as
`bin/job-server.php`. To run multiple workers in parallel, an external process
such as [supervisord](http://supervisord.org/) should be used.

The job server contains an implementation that runs on a database accessed via
a [mef\Db](https://bitbucket.org/leverton/mefworks-db/) driver, but
theoretically it could be extended to hand jobs off to some other system, such
as Gearman or Resque.

To get the server up and running:

* Run the included `etc/mysql-schema.sql`

* Make a copy of `etc/default-job-server-config.php` and make changes as
necessary. There are a few things that should be changed: `job-id-generator`
(a function that returns a unique id to assign to new jobs),
`worker-namespace` (the namespace your workers are in), and `jobs-to-run` (an
array of job names to run), and the database connection information.

* Run `bin/job-server.php --config=path-to-config.php`

The server will run until it receives a signal to stop (Ctrl-C).

License
=======

Copyright (C) 2015 Matthew Leverton

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
